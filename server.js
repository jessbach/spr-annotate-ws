var express    = require('express');
var app        = express();
var bodyParser = require('body-parser');
var Annotation = require('./app/models/annotation');

var mongoose   = require('mongoose');
mongoose.connect('mongodb://172.21.28.89:27017/annotations'); // connect to our database

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8080;        // set our port

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

router.use(function(req, res, next) {
    console.log('Something is happening.');
    next(); // make sure we go to the next routes and don't stop here
});

router.get('/', function(req, res) {
    res.json({ name: 'Annotator Store API', version: '1.0', author: 'Springer' });   
});







router.route('/annotation/:uid')
    .post(function(req, res) {
    	var uid = req.params.uid
  
		var annotation = new Annotation(); 
        annotation.payload = JSON.stringify(req.body);
        annotation.userid = uid;

        annotation.save(function(err) {
            if (err)
                res.send(err);
            console.log({ annotation: JSON.stringify(annotation) })
            res.json({ annotation: JSON.stringify(annotation) });
        });
    });

router.route('/annotation/:uid')
	.get(function(req, res) {
		var userId = req.params.uid

		Annotation.findOne({userid: userId}, function(err, obj) { 
			res.json(obj.payload);
		});
	});

router.route('/annotation/:uid/:id')
	.get(function(req, res) {
		var userId = req.params.uid
		var id = req.params.id

		Annotation.findOne({_id: id, userid: userId}, function(err, obj) { 
			if (err) return res.send(500, { error: err });
    		res.json(obj.payload);
		});
	});

router.route('/annotation/:uid/:id')
	.put(function(req, res) {
		var userId = req.params.uid
		var id = req.params.id

		var annotation 		= new Annotation(); 
        annotation.payload 	= JSON.stringify(req.body);
        annotation.userid 	= id;

		Annotation.findOneAndUpdate({_id: id, userid: userId}, annotation, {upsert:true}, function(err, doc) {
    		if (err) return res.send(500, { error: err });
    		return res.send("succesfully saved");
		});
	});

router.route('/annotation/:uid/:id')
	.delete(function(req, res) {
		var userId = req.params.uid
		var id = req.params.id

		Annotation.remove({_id: id, userid: userId}, function(err) {
		    if (err) return res.send(500, { error: err });
    		return res.send("succesfully saved");
		});
	});

// REGISTER OUR ROUTES
app.use('/', router);
// START THE SERVER
app.listen(port);
console.log('Magic happens on port ' + port);