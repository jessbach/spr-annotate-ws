var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var AnnotationSchema   = new Schema({
    payload: String,
    userid: String
});

module.exports = mongoose.model('Annotation', AnnotationSchema);